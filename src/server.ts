'use strict';

const HydraServiceFactory = require('hydra-integration').HydraServiceFactory;

import expressValidator = require('express-validator');
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import {SyncOptions} from 'sequelize';
import {ResponseHandler} from './utils/ResponseHandler';
import {MoviesAPI} from './api/MoviesAPI';
import {RatingsAPI} from './api/RatingsAPI';

const helmet = require('helmet');

const DAO = require('./dao');
const sequelize = DAO.sequelize;


export class Server {
    public app: express.Application;
    public HOSTNAME: string;
    public PORT;

    async init() {
        this.HOSTNAME = process.env.HOSTNAME || '';
        this.PORT = process.env.PORT || 8081;

        return this.initializeHydra();
    }

    async initializeHydra() {
        let factory = new HydraServiceFactory({
            hydra: {
                'serviceName': 'movies-recommendation-api',
                'serviceDescription': 'Movies Recommendation API',
                'serviceIP': this.HOSTNAME,
                'servicePort': this.PORT,
                'serviceType': 'express',
                'serviceVersion': require('../package').version,
                'redis': {
                    'host': process.env.REDIS_URL || '127.0.0.1',
                    'port': process.env.REDIS_PORT || '6379',
                    'db': process.env.REDIS_DB || '10',
                    'password': process.env.REDIS_PASSWORD || ''
                }
            }
        });

        factory = await factory.init();

        await factory.getService(service => {
            this.app = service;
            this.registerMiddlewareCallbacks();
            this.registerRouteCallbacks();
            this.registerResponseHandlers();
        });

        const hydra = factory.getHydra();

        console.log(`
              serviceName: ${hydra.serviceName} (${hydra.serviceVersion}),
              instanceId: ${hydra.instanceID},
              serviceIP: ${hydra.config.serviceIP},
              servicePort: ${hydra.config.servicePort},
             `);

        return null;
    }

    /**
     * Load configuration file and initialize hydraExpress app
     */
    registerMiddlewareCallbacks() {

        let syncOptions: SyncOptions = {};

        if (process.env.NODE_ENV !== 'production') {
            syncOptions = {
                alter: true
            }
        }

        sequelize.sync(syncOptions);

        this.app.use((helmet()));

        this.app.use((morgan('dev')));

        this.app.use(((req, res, next) => {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token, Authorization');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
            next();
        }));

        this.app.use((bodyParser.json()));
        this.app.use((bodyParser.urlencoded({extended: false, limit: '50mb'})));
        this.app.use(expressValidator());
    }

    registerRouteCallbacks() {
        this.app.use('/movie', new MoviesAPI().createRoutes());
        this.app.use('/ratings', new RatingsAPI().createRoutes());
    }

    registerResponseHandlers() {
        this.app.use((response, req, res, next) => {
            if (response instanceof ResponseHandler) {
                response.sendResponse(res);
            }
        });
    }
}


new Server().init().then(); // Start the server
