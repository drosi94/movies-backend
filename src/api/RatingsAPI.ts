'use strict';

import * as express from "express";
import {IAPI} from './IAPI';
import {RatingsController} from '../controller/RatingsController';

export class RatingsAPI implements IAPI {
  public readonly routes: express.Router;
  public controller: RatingsController;

  constructor() {
    this.routes = express.Router();
    this.controller = new RatingsController();
  }

  createRoutes(): express.Router {
    this.routes.post('/',
      this.controller.getByMovieIds
        .bind(this.controller));

    this.routes.get('/:id',
      this.controller.getByUserId
        .bind(this.controller));

    return this.routes;
  }
}
