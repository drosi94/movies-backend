'use strict';

import * as express from "express";
import {IAPI} from './IAPI';
import {MoviesController} from '../controller/MoviesController';

export class MoviesAPI implements IAPI {
  public readonly routes: express.Router;
  public controller: MoviesController;

  constructor() {
    this.routes = express.Router();
    this.controller = new MoviesController();
  }

  createRoutes(): express.Router {
    this.routes.post('/',
      this.controller.getByKeyword
        .bind(this.controller));

    this.routes.get('/:id',
      this.controller.getById
        .bind(this.controller));

    return this.routes;
  }
}
