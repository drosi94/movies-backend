import * as express from "express";

export interface IAPI {
  readonly routes: express.Router;

  createRoutes(): express.Router
}
