import {BaseModel} from './BaseModel';

export class Rating extends BaseModel {
    private _userId: number;
    private _movieId: number;
    private _rating: number;


    constructor(obj) {
        super();
        if (obj) {
            this._userId = obj.userId;
            this._movieId = obj.movieId;
            this._rating = obj.rating;
        }
    }


    get userId(): number {
        return this._userId;
    }

    set userId(value: number) {
        this._userId = value;
    }

    get movieId(): number {
        return this._movieId;
    }

    set movieId(value: number) {
        this._movieId = value;
    }

    get rating(): number {
        return this._rating;
    }

    set rating(value: number) {
        this._rating = value;
    }


    toJson() {
        return {
            movieId: this.movieId,
            userId: this.userId,
            rating: parseFloat(this.rating)
        }
    }

}