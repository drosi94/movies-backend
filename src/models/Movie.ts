import {BaseModel} from './BaseModel';

export class Movie extends BaseModel {
    private _movieId: number;
    private _title: string;
    private _genres: string;


    constructor(obj) {
        super();
        if (obj) {
            this.movieId = obj.movieId;
            this.title = obj.title;
            this.genres = obj.genres;
        }
    }

    get movieId(): number {
        return this._movieId;
    }

    set movieId(value: number) {
        this._movieId = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get genres(): string {
        return this._genres;
    }

    set genres(value: string) {
        this._genres = value;
    }


    toJson() {
        return {
            movieId: this.movieId,
            title: this.title,
            genres: this.genres
        }
    }
}