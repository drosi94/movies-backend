'use strict';
import * as fs from 'fs';
import * as path from 'path';
import * as SequelizeStatic from 'sequelize';
import {Sequelize} from 'sequelize';

export interface SequelizeModels {
    MoviesTable: SequelizeStatic.Model<any, any>;
    RatingsTable: SequelizeStatic.Model<any, any>;
}

class Database {
    private _basename: string;
    private _models: SequelizeModels;
    private _sequelize: Sequelize;

    constructor() {
        this._basename = path.basename(module.filename);

        let dbName = process.env.DB_NAME || 'db_movies';
        const dbUser = process.env.DB_USER || 'root';
        const dbPassword = process.env.DB_PASSWORD || 'asd123';
        const dbHost = process.env.DB_URL || '35.195.154.195';
        const dbPort = process.env.DB_PORT || '3306';

        this._sequelize = new SequelizeStatic(dbName, dbUser, dbPassword,
            {
                host: dbHost,
                port: parseInt(dbPort),
                dialect: 'mysql',
                pool: {
                    max: 5,
                    min: 0,
                    idle: 10000
                },
                benchmark: process.env.NODE_ENV !== 'production'
            });
        this._models = ({} as any);

        fs.readdirSync(__dirname).filter((file: string) => {
            return (file !== this._basename) && (file !== 'index.ts');
        }).forEach((file: string) => {
            let model = this._sequelize.import(path.join(__dirname, file));
            this._models[(model as any).name] = model;
        });

        Object.keys(this._models).forEach((modelName: string) => {
            if (typeof this._models[modelName].associate === 'function') {
                this._models[modelName].associate(this._models);
            }
        });
    }

    getModels() {
        return this._models;
    }

    getSequelize() {
        return this._sequelize;
    }
}

export const database = new Database();
export const models = database.getModels();
export const sequelize = database.getSequelize();
