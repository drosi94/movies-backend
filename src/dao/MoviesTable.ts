import * as SequelizeStatic from 'sequelize';
import {DataTypes, Sequelize} from 'sequelize';


export default function (sequelize: Sequelize, DataTypes: DataTypes): SequelizeStatic.Model<any, any> {
    const MoviesTable = sequelize.define<any, any>('MoviesTable', {
            movieId: {
                type: DataTypes.INTEGER(11),
                primaryKey: true,
            },
            title: {
                type: DataTypes.STRING(255)
            },
            genres: {
                type: DataTypes.STRING(255)
            }
        },
        {
            tableName: 'movies',
            name: {singular: 'movie', plural: 'movies'},
            indexes: [
                {
                    fields: ['title'],
                    unique: false,
                    method: 'HASH'
                }
            ],
            timestamps: false
        });


    return MoviesTable;
};
