import * as SequelizeStatic from 'sequelize';
import {DataTypes, Sequelize} from 'sequelize';


export default function (sequelize: Sequelize, DataTypes: DataTypes): SequelizeStatic.Model<any, any> {
    const RatingsTable = sequelize.define<any, any>('RatingsTable', {
            userId: {
                type: DataTypes.INTEGER(11)
            },
            rating: {
                type: DataTypes.DECIMAL(3,1)
            }
        },
        {
            tableName: 'ratings',
            name: {singular: 'rating', plural: 'ratings'},
            timestamps: false
        });



    RatingsTable.associate = function(models) {
        RatingsTable.belongsTo(models.MoviesTable, {foreignKey: 'movieId'})
    };


    return RatingsTable;
};
