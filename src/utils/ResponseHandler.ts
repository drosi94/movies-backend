
export class ResponseHandler {

    payload: string | {};
    status: number;

    constructor(payload: string | {}, status?: number,) {
        this.payload = payload;
        this.status = status || 500;
    }

    sendResponse(res) {
        if (this.status.toString().startsWith('4') || this.status.toString().startsWith('5')) {
            return this.sendError(res);
        } else {
            return this.sendOk(res);
        }
    }

    sendError(res) {
        if (process.env.NODE_ENV === 'production') {
            if (this.status === ResponseHandler.HTTP_SERVER_ERROR) {
                this.payload = 'INTERNAL_ERROR';
            }
        }

        const error = {
            error: this.payload
        };

        return res.status(this.status).json(error);
    }

    sendOk(res) {
        return res.status(200).json(this.payload);
    }


    /**
     * Static variables of HTTP Codes
     */
    static readonly HTTP_OK = 200;
    static readonly HTTP_CREATED = 201;
    static readonly HTTP_MOVED_PERMANENTLY = 301;
    static readonly HTTP_BAD_REQUEST = 400;
    static readonly HTTP_UNAUTHORIZED = 401;
    static readonly HTTP_PAYMENT_REQUIRED = 402;
    static readonly HTTP_NOT_FOUND = 404;
    static readonly HTTP_METHOD_NOT_ALLOWED = 405;
    static readonly NOT_ACCEPTABLE = 406;
    static readonly HTTP_CONFLICT = 409;
    static readonly HTTP_TOO_LARGE = 413;
    static readonly HTTP_TOO_MANY_REQUEST = 429;
    static readonly HTTP_SERVER_ERROR = 500;
    static readonly HTTP_METHOD_NOT_IMPLEMENTED = 501;
    static readonly HTTP_CONNECTION_REFUSED = 502;
    static readonly HTTP_SERVICE_UNAVAILABLE = 503;
}
