import {RatingsService} from '../service/RatingsService';
import {ResponseHandler} from '../utils/ResponseHandler';
import {Controller} from './Controller';
import {Rating} from '../models/Rating';

export class RatingsController extends Controller {
    service: RatingsService;
    constructor() {
        super();
        this.service = new RatingsService();
    }

    async getByMovieIds(req: Request, res: Response, next: Function) {
        try {
            let movieList = req.body.movieList;

            movieList = movieList.map( (m) => {
                return parseInt(m);
            });

            const ratings: Rating[] = await this.service.getRatingsByMovieList(movieList);

            next(new ResponseHandler(ratings.map((r: Rating) => r.toJson()), ResponseHandler.HTTP_OK));
        } catch (err) {
            console.err(err);
            next(new ResponseHandler(err.message));
        }
    }

    async getByUserId(req: Request, res: Response, next: Function) {
        try {
            const userId = req.params.id;

            const ratings: Rating[] = await this.service.getRatingsByUserId(userId);

            if (ratings.length == 0) {
                next(new ResponseHandler('Ratings with this userId did not found', ResponseHandler.HTTP_NOT_FOUND));
                return;
            }

            next(new ResponseHandler(ratings.map((r: Rating) => r.toJson()), ResponseHandler.HTTP_OK));
        } catch (err) {
            console.err(err);
            next(new ResponseHandler(err.message));
        }
    }
}