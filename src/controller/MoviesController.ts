import {Controller} from './Controller';
import {MoviesService} from '../service/MoviesService';
import {ResponseHandler} from '../utils/ResponseHandler';
import {Movie} from '../models/Movie';

export class MoviesController extends Controller {
    service: MoviesService;
    constructor() {
        super();
        this.service = new MoviesService();
    }

    async getByKeyword(req: Request, res: Response, next: Function) {
        try {
            const keyword = req.body.keyword;

            const movies: Movie[] = await this.service.getMoviesByKeyword(keyword);

            next(new ResponseHandler(movies.map((m: Movie) => m.toJson()), ResponseHandler.HTTP_OK));
        } catch (err) {
            console.err(err);
            next(new ResponseHandler(err.message));
        }
    }

    async getById(req: Request, res: Response, next: Function) {
        try {
            const id = req.params.id;

            if (id == "undefined") {
                next(new ResponseHandler('You did not provide a valid id', ResponseHandler.HTTP_BAD_REQUEST));
                return;
            }

            const movie: Movie = await this.service.getById(id);
            if (!movie) {
                next(new ResponseHandler('Movie with this id did not found', ResponseHandler.HTTP_NOT_FOUND));
                return;
            }

            const movies: Movie[] = [];
            movies.push(movie);

            next(new ResponseHandler(movies.map((m: Movie) => m.toJson()), ResponseHandler.HTTP_OK));
        } catch (err) {
            console.err(err);
            next(new ResponseHandler(err.message));
        }
    }
}