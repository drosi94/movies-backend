import {Service} from './Service';

Promise = require('bluebird');

import * as DAO from '../dao/index'
import * as Sequelize from 'sequelize';
import {Rating} from '../models/Rating';


const Op = Sequelize.Op;


export class RatingsService extends Service {
    constructor() {
        super();
        this.repository = DAO.models.RatingsTable;
    }

    async getRatingsByMovieList(movieList: number[]): Promise<Rating[]> {
        return new Promise<Rating[]>(async (resolve, reject) => {
            try {
                const result = await this.repository.findAll({
                    where: {
                        movieId: {
                            [Op.in]: movieList
                        }
                    }
                });

                const ratings: Rating[] = result.map(r => {
                    return new Rating(r);
                });

                resolve(ratings);
            } catch (err) {
                reject(err);
            }
        });
    }

    async getRatingsByUserId(userId: number): Promise<Rating[]> {
        return new Promise<Rating[]>(async (resolve, reject) => {
            try {
                const result = await this.repository.findAll({
                    where: {
                        userId: userId
                    }
                });

                const ratings: Rating[] = result.map(r => {
                    return new Rating(r);
                });

                resolve(ratings);
            } catch (err) {
                reject(err);
            }
        });
    }

}