import {Service} from './Service';

Promise = require('bluebird');

import {Movie} from '../models/Movie';

import * as DAO from '../dao/index'
import * as Sequelize from 'sequelize';


const Op = Sequelize.Op;


export class MoviesService extends Service {
    constructor() {
        super();
        this.repository = DAO.models.MoviesTable;
    }


    getById(id): Promise<Movie> {
        return new Promise<Movie>(async (resolve, reject) => {
            try {
                const result = await this.repository.findOne({
                    where: {
                        movieId: id
                    }
                });
                if (result) {
                    const movie = new Movie(result);
                    resolve(movie);
                } else {
                    resolve(null)
                }
            } catch (err) {
                reject(err);
            }
        });
    }

    async getMoviesByKeyword(keyword: string): Promise<Movie[]> {
        return new Promise<Movie[]>(async (resolve, reject) => {
            try {
                const result = await this.repository.findAll({
                    where: {
                        title: {
                            [Op.like]: '%' + keyword + '%'
                        }
                    }
                });

                const movies: Movie[] = result.map(r => {
                    return new Movie(r);
                });

                resolve(movies);
            } catch (err) {
                reject(err);
            }
        });
    }
}