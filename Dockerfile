FROM node:9.4.0-alpine
MAINTAINER bdrosatos
EXPOSE 8081
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD . /usr/src/app
ADD dist /usr/src/app/dist
RUN npm install --production
ENTRYPOINT ["node", "dist/server.js"]
